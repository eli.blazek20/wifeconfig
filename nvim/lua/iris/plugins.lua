local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	{
    		'nvim-telescope/telescope.nvim', 
		tag = '0.1.2',
      		dependencies = { 'nvim-lua/plenary.nvim' }
    	},
	{ "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" },
	{
  		"folke/tokyonight.nvim",
  		lazy = false,
  		priority = 1000,
  		opts = {},
	},
	{
  		'VonHeikemen/lsp-zero.nvim',
  		branch = 'v2.x',
  		dependencies = {
    			{'neovim/nvim-lspconfig'},             -- Required
    			{'williamboman/mason.nvim'},           -- Optional
    			{'williamboman/mason-lspconfig.nvim'}, -- Optional
    			{'hrsh7th/nvim-cmp'},     -- Required
    			{'hrsh7th/cmp-nvim-lsp'}, -- Required
    			{'L3MON4D3/LuaSnip'},     -- Required
  		}
	},
	{ "theprimeagen/harpoon" },
	{
    		'nvim-lualine/lualine.nvim',
    		opts = {
      			options = {
        			icons_enabled = false,
      			},
    		},
  	}
})
